﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_7
{
    class NumberSequence
    {
        private double[] sequence;
        private int sequenceSize;
        private SortStrategy sortStrategy;
        private SearchStrategy searchStrategy;
        private double number;
        public NumberSequence(int sequenceSize,double number)
        {
            this.sequenceSize = sequenceSize;
            this.sequence = new double[sequenceSize];
            this.number = number;
        }
        public NumberSequence(double[] array) : this(array.Length)
        {
            array.CopyTo(this.sequence, 0);
        }

        public NumberSequence(double[] array,double number){
            this.number = number;
            this.sequenceSize = array.Length;
            array.CopyTo(this.sequence, 0);
        }
        public void InsertAt(int index, double value)
        {
            this.sequence[index] = value;
        }
        public void SetSortStrategy(SortStrategy strategy)
        {
            this.sortStrategy = strategy;
        }
        public void SetSearchStrategy(SearchStrategy strategy) 
        {
            this.searchStrategy = strategy;
        }
        public void Sort() { this.sortStrategy.Sort(this.sequence); }
        public void Search() { this.searchStrategy.Search(this.sequence,this.number); }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            foreach (double element in this.sequence)
            {
                builder.Append(element).Append(Environment.NewLine);
            }
            return builder.ToString();
        }
    }
}
