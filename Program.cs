﻿using System;

namespace RPPOON_7
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] Array_to_sort = { 3, 4, 5, 10, 2, 6, 7, 1, 9, 8 };
            NumberSequence Array = new NumberSequence(Array_to_sort);
            BubbleSort SortMetode = new BubbleSort();
            Array.SetSortStrategy(SortMetode);
            Array.Sort();
            Console.WriteLine(Array.ToString());

            NumberSequence Array_2 = new NumberSequence(Array_to_sort);
            CombSort SortMetode_2 = new CombSort();
            Array_2.SetSortStrategy(SortMetode_2);
            Array_2.Sort();
            Console.WriteLine(Array_2.ToString());

            NumberSequence Array_3 = new NumberSequence(Array_to_sort);
            SequentialSort SortMetode_3 = new SequentialSort();
            Array_3.SetSortStrategy(SortMetode_3);
            Array_3.Sort();
            Console.WriteLine(Array_3.ToString());


            double[] Array_to_search = { 3, 4, 5, 10, 2, 6, 7, 1, 9, 8 };
            NumberSequence Array_4 = new NumberSequence(Array_to_search, 10);
            LinearSearch SearchMetode = new LinearSearch();
            Array_4.SetSearchStrategy(SearchMetode);
            Array_4.Search();
            Console.WriteLine(Array_4.ToString());



        }
    }
}
